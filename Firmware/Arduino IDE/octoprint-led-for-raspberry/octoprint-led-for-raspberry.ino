#include <FastLED.h>

// debug
#define DEBUG true

// pinout
#define PI_STATUS_PIN 21
#define PI_IS_PRINTING_PIN 34
#define PI_UNUSED_3 35
#define PI_STOP 19
#define PI_UNUSED_1 22
#define PI_UNUSED_2 23                 

#define BTN_TOOGLE_LIGHT 26
#define BTN_UNUSED_2 27
#define BTN_STOP 32
#define BTN_UNUSED_1 33

#define LED_STATUS 25

#define LED_STRIP_DATA_PIN 16
#define LED_STRIP_CLK_PIN 17

// led strip options
#define NUMBER_OF_LEDS 28
#define LED_TYPE WS2812B
#define COLOR_ORDER GRB
#define BLENDING_TYPE LINEARBLEND

// misc
#define RPI_BOOT_DELAY_MS 20000
#define FPS (90.0)
#define FRAME_DURATION_MS (1000.0)/FPS
#define BRIGHTNESS_STEP 1
#define MIN_BRIGHTNESS 0
#define MAX_BRIGHTNESS 255
#define TOOGLE_BUTTON_TIMEOUT_MS 500
#define DEBUG_MSG_FREQ_MS 1000

// enums
enum MODE{NOT_READY, ALMOST_READY, READY_L, READY_NL, PRINTING_L, PRINTING_NL, PRINTED, UNDEFINED};

// globals
const char *modeStrings[16] = {"NOT_READY", "ALMOST_READY", "READY_L", "READY_NL", "PRINTING_L", "PRINTING_NL", "PRINTED", "UNDEFINED"};
CRGB ledArray[NUMBER_OF_LEDS];

bool octoprintStatus()
{
  return !digitalRead(PI_STATUS_PIN);
}

bool isPrinting()
{
  return (!digitalRead(PI_IS_PRINTING_PIN));
}

bool isToogleLightPressed()
{
  static bool firstReading = true;
  static bool previousValue = true;
  static uint32_t timeoutButtonToogleLight = 0;
  
  bool value = digitalRead(BTN_TOOGLE_LIGHT);

  if(firstReading)
  {
    previousValue = value;
    firstReading = false;
    return false;
  }

  if(previousValue != value && !firstReading && millis() - timeoutButtonToogleLight > TOOGLE_BUTTON_TIMEOUT_MS)
  {
    timeoutButtonToogleLight = millis();
    previousValue = value;
    return true;
  }
  else
  {
    previousValue = value;
    return false;
  }
}

bool isStopPressed()
{
  static bool firstReading = true;
  static bool previousValue = true;
  static uint32_t timeoutButtonStop = 0;
  
  bool value = digitalRead(BTN_STOP);

  if(firstReading)
  {
    previousValue = value;
    firstReading = false;
    return false;
  }

  if(previousValue != value && !firstReading && millis() - timeoutButtonStop > TOOGLE_BUTTON_TIMEOUT_MS)
  {
    timeoutButtonStop = millis();
    previousValue = value;
    return true;
  }
  else
  {
    previousValue = value;
    return false;
  }
}

void initPins()
{
  pinMode(PI_STATUS_PIN, INPUT_PULLUP);
  pinMode(PI_IS_PRINTING_PIN, INPUT_PULLUP);
  
  pinMode(BTN_TOOGLE_LIGHT, INPUT_PULLUP);
  pinMode(BTN_STOP, INPUT_PULLUP);

  pinMode(PI_STOP, OUTPUT);
  digitalWrite(PI_STOP, HIGH);

  pinMode(LED_STATUS, OUTPUT);
  digitalWrite(LED_STATUS, LOW);
}

void setupSerials()
{
  Serial.begin(115200);
}

void initLedArray()
{
  FastLED.addLeds<LED_TYPE, LED_STRIP_DATA_PIN, COLOR_ORDER>(ledArray, NUMBER_OF_LEDS).setCorrection( TypicalLEDStrip );
  FastLED.setDither(1);
  FastLED.setBrightness(255);
  FastLED.show();
}

void ledAnimationHandler(MODE mode, bool *resetLedAnimationHandler)
{
  int16_t static brightness = MAX_BRIGHTNESS;
  bool static incBrightness = false; // indicates if we increase brightness or decrease brightness over time
  CRGB static col;

  if(*resetLedAnimationHandler)
  {
    *resetLedAnimationHandler = false;
    brightness = MAX_BRIGHTNESS;
    incBrightness = false; // indicates if we increase brightness or decrease brightness over time
  }

  if(mode == NOT_READY)
  {
    col = CRGB(255, 0, 0);
  }
  else if(mode == ALMOST_READY)
  {
    col = CRGB(255, 130, 0);
  }

  for(uint8_t i = 0; i <= NUMBER_OF_LEDS - 1; i++)
  {
    ledArray[i] = col;
  }

  FastLED.setBrightness(brightness);
  FastLED.show();

  if(incBrightness)
  {
    if(brightness + BRIGHTNESS_STEP > MAX_BRIGHTNESS)
    {
      incBrightness = false;
    }
    else
    {
      brightness += BRIGHTNESS_STEP;
    }
  }
  else
  {
    if(brightness - BRIGHTNESS_STEP < MIN_BRIGHTNESS)
    {
      incBrightness = true;
    }
    else
    {
      brightness -= BRIGHTNESS_STEP;
    }
  }
}

void ledStaticHandler(MODE mode)
{
  CRGB col = CRGB(0, 0, 0);

  if(mode == PRINTING_L || mode == READY_L)
  {
    col = CRGB(255, 255, 255);
  }
  else if(mode == PRINTING_NL || mode == READY_NL)
  {
    col = CRGB(0, 0, 0);
  }

  for(uint8_t i = 0; i <= NUMBER_OF_LEDS - 1; i++)
  {
    ledArray[i] = col;
  }

  FastLED.setBrightness(MAX_BRIGHTNESS);
  FastLED.show();
}

void setup() 
{
  delay(3000);
  setupSerials();

  if(DEBUG)
  {
    Serial.print("SETUP ... ");
  }

  initPins();
  initLedArray();

  if(DEBUG)
  {
    Serial.println("OK"); 
  }
}

void loop() 
{
  static MODE mode = NOT_READY;
  static bool resetLedAnimationHandler = false;

  // timers
  static uint32_t octoprintBootDelay_t = 0;
  static uint32_t fps_t = 0;
  static uint32_t debug_t = 0;
  static uint32_t stop_t = 0;
  
  // check for octoprint startup
  if(mode == NOT_READY && octoprintStatus())
  {
    mode = ALMOST_READY;
    octoprintBootDelay_t = millis();
  }
  // check if certain amount of ms passed, because octoprint reports startup too early, it´s not ready
  
  if(mode == ALMOST_READY && millis() - octoprintBootDelay_t >= RPI_BOOT_DELAY_MS)
  {
    mode = READY_NL;
  }
  
  if((mode == ALMOST_READY || mode == READY_L || mode == READY_NL || mode == PRINTING_L || mode == PRINTING_NL || mode == PRINTED) && !octoprintStatus())
  {
    mode = NOT_READY;
  }
  
  // check for beginning of the print
  if(isPrinting() && (mode == READY_L || mode == READY_NL))
  {
    mode = PRINTING_L;
    resetLedAnimationHandler = true;
  }

  // check for the end of the print
  if(mode == PRINTING_L || mode == PRINTING_NL)
  {
    // check if we need to stop the print
    if(isStopPressed())
    {
      digitalWrite(PI_STOP, LOW);
      stop_t = millis();
    }
    
    // check if print finished
    if(!isPrinting())
    {
      mode = PRINTED;
    }
  }

  if(millis() - stop_t > 1000)
  {
    digitalWrite(PI_STOP, HIGH);
  }

  if(mode == PRINTED)
  {
    // TODO: add some 3-5s LED indication that print finished (add some new state), then go to READY_NL state

    mode = READY_NL;
  }

  // update LED_STATUS
  if(mode == NOT_READY || mode == ALMOST_READY)
  {
    digitalWrite(LED_STATUS, LOW);
  }
  else
  {
    digitalWrite(LED_STATUS, HIGH);
  }

  // handle color + animation
  if((mode == NOT_READY || mode == ALMOST_READY) && millis() - fps_t >= FRAME_DURATION_MS)
  {
    fps_t = millis();
    ledAnimationHandler(mode, &resetLedAnimationHandler);
  }
  else if(mode == READY_L || mode == READY_NL || mode == PRINTING_L || mode == PRINTING_NL)
  { 
    if(isToogleLightPressed())
    {
      if(mode == READY_NL)
      {
        mode = READY_L;
      }
      else if(mode == READY_L)  
      {
        mode = READY_NL;
      }
      else if(mode == PRINTING_NL)
      {
        mode = PRINTING_L;
      }
      else if(mode == PRINTING_L)  
      {
        mode = PRINTING_NL;
      }
    }
    
    ledStaticHandler(mode);
  }

  if(DEBUG && millis() - debug_t > DEBUG_MSG_FREQ_MS)
  {
    debug_t = millis();
    Serial.println(modeStrings[mode]);  
  }
}